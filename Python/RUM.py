# RUM Settings App Trigger Handlers
# Created by Keegan for RebornOS and Arch Linux
# This is an open-source project using Python3.  Feel free to use
# what you'd like, but please give credit!  Improvements are always welcome!
# RebornOS Discord: Keegan

# Import necessary modules
import subprocess
import gi
import os
import json
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
gi.require_version('Notify', '0.7')
from gi.repository import Notify
Notify.init("Reborn Updates and Maintenance")
try:
    import httplib
except:
    import http.client as httplib

# Create variables for the current working directory, the settings file, and the location of the RebornRUM / RUM file
workingDirectory = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..'))
settingsFile = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'settings.json'))
RebornRUMfile = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'RebornMaintenance'))
RUMfile = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'Maintenance'))

if not os.path.isfile(settingsFile):
    import RebornSettings
else:
    with open(settingsFile) as outfile:
        settings = json.load(outfile)
        #RebornSettings

# RUM Settings App Trigger Handlers
# Created by Keegan for RebornOS and Arch Linux
# This is an open-source project using Python3.  Feel free to use
# what you'd like, but please give credit!  Improvements are always welcome!
# RebornOS Discord: Keegan

# Import necessary modules
import subprocess
import gi
import os
import json
import fileinput
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
gi.require_version('Notify', '0.7')
from gi.repository import Notify
Notify.init("Reborn Updates and Maintenance")
try:
    import httplib
except:
    import http.client as httplib

# Create variables for both the current working directory and the location of the settings file
workingDirectory = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..'))
settingsFile = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'settings.json'))

# Check for Internet connection
conn = httplib.HTTPConnection("www.google.com", timeout=5)
try:
    conn.request("HEAD", "/")
    conn.close()
except:
    conn.close()
    Notify.Notification.new("Lacking Internet connection. Some options may not work").show()

# Create Handlers (Triggers) for each item
class Handler:
    def __init__(self):
        # Create settings file is it does not already exist, as well as declare the self.settings array
        if not os.path.isfile(settingsFile):
            with open(settingsFile, 'w+') as outfile:
                self.settings = [["RebornOS", 1],["Downgrade", 1],[""]]
                outfile.write(json.dumps(self.settings))
        else:
            with open(settingsFile) as outfile:
                self.settings = json.load(outfile)

# Close the window
    def onDestroy(self, *args):
        Gtk.main_quit()

################################################################################
############################### Controls for Settings ##################################
################################################################################

    def setReborn(self, switch, state):
        if state == True:
            self.settings[0]=["RebornOS", 1]
        else:
            self.settings[0]=["RebornOS", 0]

        with open(settingsFile, 'w+') as outfile:
            outfile.write(json.dumps(self.settings))

    def setEnableDowngrade(self, switch, state):
        if state == True:
            self.settings[1]=["Downgrade", 1]
        else:
            self.settings[1]=["Downgrade", 0]

        with open(settingsFile, 'w+') as outfile:
            outfile.write(json.dumps(self.settings))

################################################################################
############################### ComboBox Terminal Settings ############################
################################################################################

    def setTerminal(self, combo):
        tree_iter = combo.get_active_iter()
        if tree_iter is not None:
            model = combo.get_model()
            chosenTerminal = model[tree_iter][0]
            if chosenTerminal == "Deepin Terminal":
                print("Deepin Terminal successfully selected!")
                self.settings[2]=["deepin-terminal --execute "]
                with open(settingsFile, 'w+') as outfile:
                    outfile.write(json.dumps(self.settings))

            elif chosenTerminal == "GNOME Terminal":
                print("GNOME Terminal successfully selected!")
                self.settings[2]=["gnome-terminal -- "]
                with open(settingsFile, 'w+') as outfile:
                    outfile.write(json.dumps(self.settings))

            elif chosenTerminal == "Konsole":
                print("Konsole Terminal successfully selected!")
                self.settings[2]=["konsole -e "]
                with open(settingsFile, 'w+') as outfile:
                    outfile.write(json.dumps(self.settings))

            elif chosenTerminal == "Xterm":
                print("Xterm Terminal successfully selected!")
                self.settings[2]=["xterm -e "]
                with open(settingsFile, 'w+') as outfile:
                    outfile.write(json.dumps(self.settings))

            elif chosenTerminal == "Select Terminal...":
                Notify.Notification.new("Please select a terminal").show()
            else:
                Notify.Notification.new("ERROR! Abort! Abort!!!").show()

################################################################################
############################### Drawing App Window #################################
################################################################################

builder = Gtk.Builder()
builder.add_from_file(workingDirectory + "/Glade/RebornSettings.glade")
builder.connect_signals(Handler())

window = builder.get_object("RebornSettings")
window.show_all()

Gtk.main()

Notify.uninit()

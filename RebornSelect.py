# Reborn Maintenance App Trigger Handlers
# Created by Azaiel for RebornOS
# This is an open-source project using Python3.  Feel free to use
# what you'd like, but please give credit!  Improvements are always welcome!
# RebornOS Discord: Azaiel

# This ensures that the Gtk version is 3.0
import subprocess
import gi
import os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
gi.require_version('Notify', '0.7')
from gi.repository import Notify
Notify.init("Unnecessary Packages")
try:
    import httplib
except:
    import http.client as httplib

# Check for Internet connection
conn = httplib.HTTPConnection("www.google.com", timeout=5)
try:
    conn.request("HEAD", "/")
    conn.close()
except:
    conn.close()
    Notify.Notification.new("Lacking Internet connection. The following operations will not work").show()

workingDirectory = os.path.dirname(os.path.realpath(__file__))

# Create Handlers (Triggers) for each item
class Handler:

# Close the window
    def onDestroy3(self, *args):
        Gtk.main_quit()

################################################################################
############################### Buttons ########################################
################################################################################

# Save Program List
    def onDirectoryChosen(self, menuitem):
        global filechosen
        filechosen = menuitem.get_file().get_path()
        print("selected folder: ", filechosen)
        print()

# Save To
    def onSaveTo(self, button):
        print("selected folder: ", filechosen)
        print()
        Notify.Notification.new("Please be patient. This could take a few minutes...").show()
        #os.system('touch /tmp/saving.txt')
        #f = open('/tmp/saving.txt','w')
        #f.write(filechosen)
        os.system('pacman -Qqen > ' + filechosen + '/packages-repository.txt')
        os.system('pacman -Qqem > ' + filechosen + '/packages-AUR.txt')
        Gtk.main_quit()

# Recover From
    def onRecoverFrom(self, button):
        print("selected folder: ", filechosen)
        Notify.Notification.new("Please be patient. This could take a few minutes...").show()
        #os.system('touch /tmp/saving.txt')
        #f = open('/tmp/saving.txt','w')
        #f.write(filechosen)
        os.system('xterm -e sudo pacman --needed -S - < ' + filechosen + '/packages-repository.txt --noconfirm')
        os.system('xterm -e cat ' + filechosen + '/packages-AUR.txt | xargs yay -S --needed --noconfirm')
        Gtk.main_quit()

################################################################################
############################### Drawing App Window #############################
################################################################################

builder = Gtk.Builder()
builder.add_from_file(workingDirectory + "/RebornMaintenance.glade")
builder.connect_signals(Handler())

window1 = builder.get_object("Reborn3")
window1.show_all()

Gtk.main()

Notify.uninit()
